import numpy as np
from nonLinear import nonLinear

def backProp(input, output, hiddenLayer, weight):
    error = output - hiddenLayer
    delta = error * nonLinear(hiddenLayer, True)

    # update bobot
    return weight + np.dot(input.T,delta)