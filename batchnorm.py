import numpy as np

#question num 3c
def batchnorm_forward(x,gm,bt,eps,mm,rm,rv):
    #Mini Batch
    sum = 0
    mean = np.mean(x)
    var = np.var(x)

    #normalization
    xmu = x - mean
    std = np.sqrt(var+eps)
    xi = xmu/std
    out = gm * xi + bt

    #updating running mean & var
    rm = mm * rm + (1.0-mm) * mean
    rv = mm * rv + (1.0-mm) * var

    return out,xmu,std,xi,mm,rm,rv

#question num 3d
def batchnorm_backward(dout,gm,xmu,std,eps,xi):
    N=len(dout)
    dbeta = np.sum(dout)
    dgamma = np.sum(dout)*xi
    dx = 1/N * gm * 1/std * (N*dout - dbeta - (xmu * 1/np.sqr(std) * np.sum(dout * xmu)))

    return dx,dgamma,dbeta

#question num 3e
def batchnorm_test(x,gm,bt,eps,rm,rv):
    xHat = (x - rm) / np.sqrt(rv+eps)
    out = gm * xHat + bt
    return out