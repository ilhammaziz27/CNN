import numpy as np
from nonLinear import nonLinear

def feedForward(inputs, weight):
    for i in xrange(10000):
        # FeedForward
        layer0 = inputs
        layer1 = nonLinear(np.dot(layer0, weight))

        return  layer1

