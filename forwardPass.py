import numpy as np

#question num 3a

def sigmoid(x):
    return 1/(1+np.exp(-x))

def tanh(x):
    return (np.exp(2*x)-1)/((np.exp(2*x)+1))

def relu(x):
    if(0<x):
        return x
    else:
        return 0

def elu(x):
    if(x>0):
        return x
    else:
        return (np.exp(x)-1)