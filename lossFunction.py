import numpy as np

def softmax(input,weight,bias,output,n):
    scores = np.dot(input,weight) + bias
    exp_scores = np.exp(scores)
    prob = exp_scores / np.sum  (exp_scores,axis=1,keepdims=True)

    crossEntropy = -np.log(prob[range(n),output])

    data_loss = np.sum(crossEntropy) / n
    reg_loss = 0.5 * data_loss * np.sum(weight * weight)
    loss = data_loss + reg_loss
    return loss

def svm(x,w,b,y):
    lr=1.0
    scores = np.dot(x, w) + b
    correct_score = scores[y]
    Li=0/0
    D=w.shape[0]

    for j in xrange(D):
        if j==y:
            continue

        Li += max(0,scores[j] - correct_score + lr)
        return Li